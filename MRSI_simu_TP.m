
%load the different mask and visualise the 
load('mask_CSF.mat')
load('mask_GM.mat')
load('mask_Lip.mat')
load('mask_WM.mat')
colormap gray
subplot(2,2,1)
imagesc(mask_CSF)
title('mask_{CSF}')
subplot(2,2,2)
imagesc(mask_WM)
title('mask_{WM}')
subplot(2,2,3)
imagesc(mask_GM)
title('mask_{GM}')
subplot(2,2,4)
imagesc(mask_Lip)
title('mask_{Lip}')


%%
% create your High resolution  MRSI grid Space (Image, t)
[entete, sigPCr]=read_LCMRAW("Cr.RAW");
[entete, sigPCho]=read_LCMRAW("Pcho.RAW");
[entete, sigNAA]=read_LCMRAW("NAA.RAW");
[entete, sigLac]=read_LCMRAW("Lac.RAW");

N1=size(mask_GM,1);
N2=size(mask_GM,2);
Wm_prop=[8 3 10 1];
Gm_prop=[8 2 10 1];

apo= 10;
N=length(sigPCr);
dt=0.0004;
tt=[0:dt:(N-1)*dt].';

% generate a spectropic signal with correct concentrations of metabolites
sigwm=sum([sigPCr;sigPCho;sigNAA;sigLac].'*Wm_prop.',2).*exp(-apo*tt);
sigm=sum([sigPCr;sigPCho;sigNAA;sigLac].'*Gm_prop.',2).*exp(-apo*tt);

% copy with signal each point of the mask
% first, create an image with the same dimension of the mask
% we find the spectroscopic signal in all the voxels
gmgrid=repmat(reshape(sigm,[1 1 N]),[size(mask_GM,1) size(mask_GM,2) 1]); 
wmgrid=repmat(reshape(sigwm,[1 1 N]),[size(mask_WM,1) size(mask_WM,2) 1]); 
% second, apply mask on these images to have only signal in voxel of interest
csiGM=repmat(mask_GM,[1 1 N]).*gmgrid;
csiWM=repmat(mask_WM,[1 1 N]).*wmgrid;

%% for subcutaneous tissue
lip=generateLipids(dt,str2num(entete.HZPPM),20,N);
lipgrid=repmat(reshape(lip,[1 1 N]),[size(mask_GM,1) size(mask_GM,2) 1]); 
csilip=repmat(mask_Lip,[1 1 N]).*lipgrid;

%%
% %create MRSI grid per mask high resolution
% csilip=repmat(mask_Lip,[1 1 N]).*lipgrid;
% csiGM=repmat(mask_GM,[1 1 N]).*gmgrid;
% csiWM=repmat(mask_WM,[1 1 N]).*wmgrid;

%csi_final=csiGM+csiWM+noise;
sigma=0.5;
noise=sigma*randn(N1,N2,N);
csi_final=csilip+csiGM+csiWM+noise;
%%
% Go to the k,t space
N1=size(mask_GM,1);
N2=size(mask_GM,2);

for k=1:N
    kcsi(:,:,k)=fftshift(fft2((csi_final(:,:,k))));
end

% rechercher dans sur le premier point temporel de kcsi le voxel de maximum
% intensité (valeur absolue du signal)

%
mx=max(max(squeeze(abs(kcsi(:,:,1)))));
imagesc(squeeze(real(kcsi(:,:,1))))

% A quoi correspond ce spectre? 
[I,J]=find(squeeze(abs(kcsi(:,:,1))==mx));
plot(real(fftshift(fft(squeeze(kcsi(I,J,:))))))

%%

%troncature 2
kcsitr2=kcsi(max(I-N1/16,1):min(I+N1/16, N1),max(J-N2/16,1):min(J+N2/16, N2),:);

%retour à l'espace image

for k=1:N
csilr2(:,:,k)=ifft2(ifftshift((kcsitr2(:,:,k))));
end

subplot(1,2,1)
imagesc(squeeze(real(csi_final(:,:,1))))
subplot(1,2,2)
imagesc(squeeze(real(csilr2(:,:,1))))


Ni1=size(kcsitr2,1);
Ni2=size(kcsitr2,2);
for k=1:Ni1
    for l=1:Ni2
        csift2(k,l,:)=fftshift(fft(squeeze(csilr2(k,l,:))));
    end
end
figure, plot(real(squeeze(csift2(19,10,:))))
hold on, plot(real(squeeze(csift2(19,11,:))))


figure,
subplot(1,2,1)
imagesc(real(squeeze(csift2(:,:,1369))))
subplot(1,2,2)
imagesc(angle(squeeze(csift2(:,:,1369))))
%%
mask_WM =(ima == 3)
kmask_test=fftshift(fft2(mask_Lip));
%fftshift(fft2(mask_WM+mask_GM));



test= zeros(N1,N2);
test(max(I-N1/16,1):min(I+N1/16, N1),max(J-N2/16,1):min(J+N2/16))=kmask_test(max(I-N1/16,1):min(I+N1/16, N1),max(J-N2/16,1):min(J+N2/16));
test2=kmask_test(max(I-N1/16,1):min(I+N1/16, N1),max(J-N2/16,1):min(J+N2/16));
itest=ifft2(ifftshift(test));
itest2=ifft2(ifftshift(test2));

%%