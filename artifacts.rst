Artifacts
=========

*Section authors:* Julien Lamy <lamy@unistra.fr>, Franck Mauconduit <franck.mauconduit@cea.fr>


B₀ Inhomogeneity
----------------

Effects of B₀ Inhomogeneity
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Create a phantom object similar to the one in the 1D spatial encoding section.

.. tab:: Python
    
    .. literalinclude:: artifacts_B0.py
        :lines: 9-23
        :dedent: 4
.. tab:: MATLAB
    
    .. literalinclude:: artifacts_B0.m
        :lines: 1-26
        :language: matlab

Create a function to simulate a simple gradient echo sequence in 1D: it will look like the previous simulations, but will also take the $B_0$ offset (in Hz) and the echo time (in seconds), as arguments.

.. solution::
    
    .. tab:: Python
        
        .. literalinclude:: artifacts_B0.py
            :pyobject: simulate
    .. tab:: MATLAB
        
        .. literalinclude:: artifacts_B0.m
            :lines: 137-163
            :language: matlab

Using a dwell time of 10 µs, compute the k-space, trajectory and gradient, and, setting the echo time to 5 ms, simulate the sequence without a $B_0$ offset.

.. solution::
    
    .. tab:: Python
        
        .. literalinclude:: artifacts_B0.py
            :lines: 25-38
            :dedent: 4
    .. tab:: MATLAB
        
        .. literalinclude:: artifacts_B0.m
            :lines: 28-40
            :language: matlab

We will start by simulating a spatially constant $B_0$ offset of 5 ppm at a field of 7 T. What is the corresponding frequency offset, in Hz?

Simulate the signal, reconstruct the image and plot the original signal and the image, with both magnitude and phase, with a null $B_0$ offset and with a constant $B_0$ offset. Describe and explain the results.

.. solution::
    
    .. tab:: Python
        
        .. literalinclude:: artifacts_B0.py
            :lines: 40-62
            :dedent: 4
    
    .. tab:: MATLAB
        
        .. literalinclude:: artifacts_B0.m
            :lines: 42-66
            :language: matlab
    
    The frequency offset is $\gamma/(2\pi) \times 7 \times 5 \cdot 10^{-6} = $1490 Hz.
    
    .. image:: artifacts_B0_constant.png
    
    The magnitude of both signal match, but the phase is modified. Although it may be difficult to see on the picture, as the phase imparted by the spatial encoding gradient causes large jumps, there is a time-dependent, linear drift imparted on the phase. Due to the duality of the Fourier transform between shifting and a phase ramp, this phase modification creates a translated image, with the same intensity, and with a constant phase shift.

Run the simulation again but with a spatially-dependent $B_0$ inhomogeneity, with -10 ppm in the first object, +5 ppm in the second and +10 ppm in the third. Explain and discuss the results.

.. solution::
    
    .. tab:: Python
        
        .. literalinclude:: artifacts_B0.py
            :lines: 65-87
            :dedent: 4
    
    .. tab:: MATLAB
        
        .. literalinclude:: artifacts_B0.m
            :lines: 68-94
            :language: matlab
    
    .. image:: artifacts_B0_by_object.png
    
    Since the $B_0$ offset varies spatially, this behaves as if an extra spatial-encoding gradient was applied, and the whole k-space is slightly shifted, with a more complex phase modification.
    
    In the image, each object is shifted differently: the leftmost and rightmost objects are moved by the same amount but in different directions, while the middle object and rightmost object are moved by different amount but in same directions. This implies that the spatial shift is directly related to the $B_0$ offset.

Increase the echo time to 5.1 ms and re-run the simulation. Compare with the previous results.

.. solution::
    
    .. tab:: Python
        
        .. literalinclude:: artifacts_B0.py
            :lines: 89-107
            :dedent: 4
    
    .. tab:: MATLAB
        
        .. literalinclude:: artifacts_B0.m
            :lines: 96-117
            :language: matlab
    
    .. image:: artifacts_B0_long.png
    
    With an increased echo time, the spatial shift in the image is not modified, although the attenuation due to relaxation is becoming visible. The difference in the image phase between the two conditions is however larger than the previous example. This behavior implies that the spatial shifts will depend on timing parameters, and hint at a method to measure the $B_0$ inhomogeneity.

Measure of B₀ Inhomogeneity
~~~~~~~~~~~~~~~~~~~~~~~~~~~

In the duration $\tau$ between the RF pulse and the start of the readout, the magnetization will precess by $\gamma \Delta B_0 \tau$ rad. If we use two acquisitions with different echo times separated by $\Delta{TE}$, or alternatively run an acquisitions with two echoes, the phase shift between the two images will be $\gamma \Delta B_0 \Delta{TE}$. Compute the expected phase shift given by a $\Delta B_0$ of 5 ppm at 7 T with a $\Delta{TE}$ of 0.1 ms. Simulate the corresponding imagescompute the spatial map of $\Delta{B_0}$ in radians and in ppm, and plot it.

.. solution::
    
    The phase shift in the specified conditions is 0.94 rad. This is the interval visible in the phase of the middle object using echo times of 5 ms and 5.1 ms
    
    .. tab:: Python
        
        .. literalinclude:: artifacts_B0.py
            :lines: 110-130
            :dedent: 4
    
    .. tab:: MATLAB
        
        .. literalinclude:: artifacts_B0.m
            :lines: 119-135
            :language: matlab
    
    .. image:: artifacts_B0_map.png
    
    The different values of $\Delta B_0$ per object are clearly visible. Note that simply taking the difference of the phase may lead to increased noise in the $B_0$ map: it is better to use the complex signal and compute the phase of $z_{TE_2} \cdot z_{TE_1}^*$. For similar reasons, using $z_{TE_2} / z_{TE_1}$ would also lead to an increased noise.

B₁ Inhomogeneity
----------------

Effects of B₁ Inhomogeneity
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Using the same phantom as in the previous section, create a function to simulate a simple gradient echo sequence in 1D: it will use a fixed echo time of 5 ms, but will take the multiplicative $B_1$ inhomogeneity (unitless) and the flip angle (rad), as arguments.

.. solution::
    
    .. tab:: Python
        
        .. literalinclude:: artifacts_B1.py
            :pyobject: simulate
    .. tab:: MATLAB
        
        .. literalinclude:: artifacts_B1.m
            :lines: 97-123
            :language: matlab

Simulate the sequence for a constant $B_1$ inhomogenity of 1 (i.e. no modification of the nominal flip angle) and 0.9, with a flip angle of 20°. Plot the signal and the magnitude of the simulations and discuss the results.

.. solution::
    
    .. tab:: Python
        
        .. literalinclude:: artifacts_B1.py
            :lines: 25-52
            :dedent: 4
    .. tab:: MATLAB
        
        .. literalinclude:: artifacts_B1.m
            :lines: 28-52
            :language: matlab
    
    .. image:: artifacts_B1_constant.png
    
    Since the flip angle is not at its nominal value, the longitudinal magnetization flipped to the transversal plane is different -- in this case, lower. This is visible on both the signal and the image.

Create a relative $B_1$ of 0.7 in the first object, 1.1 in the second, and 1.3 in the third. Re-run the simulation.

.. solution::
    
    .. tab:: Python
        
        .. literalinclude:: artifacts_B1.py
            :lines: 55-68
            :dedent: 4
    .. tab:: MATLAB
        
        .. literalinclude:: artifacts_B1.m
            :lines: 54-67
            :language: matlab
    
    .. image:: artifacts_B1_by_object.png
    
    With a spatially-dependent $B_1$ inhomogeneity, the contrast of the image will be affected.

Run the simulation with a flip angle of 40° and explain the difference.

.. solution::
    
    .. tab:: Python
        
        .. literalinclude:: artifacts_B1.py
            :lines: 71-82
            :dedent: 4
    .. tab:: MATLAB
        
        .. literalinclude:: artifacts_B1.m
            :lines: 69-78
            :language: matlab
    
    .. image:: artifacts_B1_double_angle.png
    
    The magnetization tipped from the longitudinal axis to the transversal plane depends on the actual flip angle, i.e. the product of the nominal flip angle and the $B_1$ inhomogeneity.

Measure of B₁ Inhomogeneity
~~~~~~~~~~~~~~~~~~~~~~~~~~~

By assuming that the $B_1$ inhomogeneity does not depend on the nominal flip angle, it is easy to devise a mapping method. The signal ratio of two acquisitions with different flip angles but with all other parameters equal will depend solely on the quantity of magnetization tipped in the transversal plane, i.e. $r = I_1 / I_2 = \sin(\theta_1) / \sin(\theta_2)$. Since $\sin(2\theta) = 2 \sin(\theta)\cos(\theta)$, the ratio of two images with one flip angle being the double of the other one is $r = 2\cos(\theta)$.

Compute the map of the $B_1$ inhomogeneity using this method, with flip angles of 20° and 40°.

.. solution::
    
    .. tab:: Python
        
        .. literalinclude:: artifacts_B1.py
            :lines: 85-104
            :dedent: 4
    .. tab:: MATLAB
        
        .. literalinclude:: artifacts_B1.m
            :lines: 80-95
            :language: matlab
    
    .. image:: artifacts_B1_map.png


Chemical Shift
--------------

Create an object with three regions which have the same $M_0$, $T_1$, and $T_2$, but have different $\Delta\omega$ values: the innermost region is on resonance ($\Delta\omega$ = 0 ppm) while the two outermost regions have the frequency of fat (3.5 ppm, we will assume a field of 1.5 T in this simulation).

.. tab:: Python
        
    .. literalinclude:: chemical_shift.py
        :lines: 9-27
        :dedent: 4
.. tab:: MATLAB
    
    .. literalinclude:: chemical_shift.m
        :lines: 1-32
        :language: matlab

Compute the trajectory and simulate the images with a dwell time of 50 µs and of 200 µs. Plot the magnitude of the images and explain the results

.. solution::
    
    .. tab:: Python
        
        .. literalinclude:: chemical_shift.py
            :lines: 29-57
            :dedent: 4
    
    .. tab:: MATLAB
        
        .. literalinclude:: chemical_shift.m
            :lines: 34-57
            :language: matlab
    
    .. image:: chemical_shift.png
    
    The magnitude of the image is mostly constant due to the lack of contrast between regions. At the boundaries of the regions, there are however signal jumps: the frequency offset of the outermost regions behaves as if there was a $B_0$ inhomongenity, since both are related to precession frequency. The positive peak between the leftmost object and the central object corresponds to an overlap of the signals: the signal from the leftmost object displaced to the right and superimposes with the signal from the central obejct. Similarly, the negative peak between the central object and the rightmost object correspons to a signal loss: the signal from the rightmost object is displaced to the right, and there is no apparent signal between the central object and the rightmost object.

The magnitude of this so called chemical shift artifact depends on the frequency offset and on the readout time: $\Delta x = \Delta\omega \cdot \tau_{readout}$. Compute the shift under the two previous conditions, and compute the total readout time and dwell time which give a shift of 0.5 pixels. What gradient amplitude is required to reach this dwell time? 

.. solution::
    
    .. tab:: Python
        
        .. literalinclude:: chemical_shift.py
            :lines: 60-73
            :dedent: 4
    
    .. tab:: MATLAB
        
        .. literalinclude:: chemical_shift.m
            :lines: 59-72
            :language: matlab
    
    .. image:: chemical_shift.png
    
    The shifts are 1.68 pixels and 6.71 for dwell times of 50 µs and 200 µs, in line with the observations on the previous plots.
    
    For a shift of 0.5 pixel, a maximum readout time of 2.24 ms is required, i.e. a dwell time of 14.91 µs. With the current experimental conditions, the gradient amplitude must be at least 10.5 mT/m.
