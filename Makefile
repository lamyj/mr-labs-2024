SPHINXOPTS    ?=
SPHINXBUILD   ?= sphinx-build
SOURCEDIR     = .
BUILDDIR      = _build

KATEX := https://github.com/KaTeX/KaTeX/releases/download
KATEX_VERSION := v0.16.9

.PHONY: html clean

all: html

_static/katex/katex.min.js:
	[ ! -d _static/katex ] & mkdir -p _static/katex
	curl -L $(KATEX)/$(KATEX_VERSION)/katex.tar.gz | tar -x -z -C _static/

html: Makefile _static/katex/katex.min.js \
		saturation_recovery.png \
		\
		object_1d.png signal_1d.png image_1d.png image_1d_T2.png \
			image_1d_aliased.png signal_1d_long_dwell_time.png \
			image_1d_long_dwell_time.png \
		object_2d.png trajectory_zig_zag.png signal_zig_zag.png \
			image_zig_zag.png signal_zig_zag_long_dwell_time.png \
			image_zig_zag_long_dwell_time.png \
		\
		artifacts_B0_constant.png artifacts_B0_by_object.png \
			artifacts_B0_long.png artifacts_B0_map.png\
		\
		artifacts_B1_constant.png artifacts_B1_by_object.png \
			artifacts_B1_double_angle.png artifacts_B1_map.png \
		\
		chemical_shift.png \
		\
		quantification_T1_BM.png quantification_T1_BM_noisy.png \
			quantification_T1_fit.png quantification_T1_monte_carlo.png 
	@$(SPHINXBUILD) -b $@ "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)

clean: Makefile
	@$(SPHINXBUILD) -M $@ "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)

saturation_recovery.png: saturation_recovery.py simulator.py style.py
	python3 $<

object_1d.png signal_1d.png image_1d.png image_1d_T2.png image_1d_aliased.png \
			signal_1d_long_dwell_time.png image_1d_long_dwell_time.png \
		&: spatial_encoding_1d.py simulator.py style.py
	python3 $<

object_2d.png trajectory_zig_zag.png signal_zig_zag.png image_zig_zag.png \
			signal_zig_zag_long_dwell_time.png \
			image_zig_zag_long_dwell_time.png \
		&: spatial_encoding_zig_zag.py simulator.py style.py
	python3 $<

artifacts_B0_constant.png artifacts_B0_by_object.png artifacts_B0_long.png \
			artifacts_B0_map.png \
		&: artifacts_B0.py simulator.py style.py
	python3 $<

artifacts_B1_constant.png artifacts_B1_by_object.png \
			artifacts_B1_double_angle.png artifacts_B1_map.png \
		&: artifacts_B1.py simulator.py style.py
	python3 $<

chemical_shift.png &: chemical_shift.py simulator.py style.py
	python3 $<

quantification_T1_BM.png quantification_T1_BM_noisy.png \
			quantification_T1_fit.png quantification_T1_monte_carlo.png \
		&: quantification_T1.py style.py
	python3 $<
