radius = 0.02; % m
centers = [-0.05, 0., 0.05]; % m

fov = 0.15; % m
voxel_size = 1e-3; % m
definition = fov/voxel_size; % voxels

object_length = 2*radius/voxel_size; % number of voxels in each object
positions = [];
for i=1:length(centers)
    positions = [ ...
        positions, voxel_size * (-object_length/2:object_length/2-1)+centers(i)];
end

M0 = [ ...
    repelem(1, object_length), ...
    repelem(0.7, object_length), ...
    repelem(0.5, object_length)]; % arbitrary units
T1 = [ ...
    repelem(1, object_length), ...
    repelem(0.7, object_length), ...
    repelem(2.0, object_length)]; % s
T2 = [ ...
    repelem(10e-3, object_length), ...
    repelem(50e-3, object_length), ...
    repelem(100e-3, object_length)]; % s

k_definition = definition; % unitless
k_max = 0.5 * 2*pi/voxel_size; % rad/m
delta_k = 2*k_max/(k_definition-1); % rad/m
trajectory = linspace(-k_max, k_max, k_definition);

dwell_time = 10e-6 ; % s
G = diff(trajectory)/(Simulator.gamma*dwell_time) ; % T/m

delta_omega = zeros(1, length(positions));
signal_no_delta_omega = simulate( ...
    M0, T1, T2, positions, delta_omega, ...
    5e-3, trajectory, G, delta_k, dwell_time);
image_no_delta_omega = fftshift(fft(fftshift(signal_no_delta_omega)));

epsilon = Simulator.gamma_bar * 7 * 5e-6; % Hz
delta_omega = repelem(epsilon, length(positions));

signal_with_delta_omega = simulate( ...
    M0, T1, T2, positions, delta_omega, ...
    5e-3, trajectory, G, delta_k, dwell_time);
image_constant_delta_omega = fftshift(fft(fftshift(signal_with_delta_omega)));

figure;
subplot(2, 2, 1); hold on;
plot(abs(signal_no_delta_omega));
plot(abs(signal_with_delta_omega));
hold off;
subplot(2, 2, 2); hold on;
plot(angle(signal_no_delta_omega));
plot(angle(signal_with_delta_omega));
hold off;
subplot(2, 2, 3); hold on;
plot(abs(image_no_delta_omega));
plot(abs(image_constant_delta_omega));
hold off;
subplot(2, 2, 4); hold on;
plot(angle(image_no_delta_omega));
plot(angle(image_constant_delta_omega));
hold off;

delta_omega = [ ...
    repelem(-2*epsilon, object_length), ...
    repelem(epsilon, object_length), ...
    repelem(2*epsilon, object_length)];

signal_with_delta_omega = simulate( ...
    M0, T1, T2, positions, delta_omega, ...
    5e-3, trajectory, G, delta_k, dwell_time);
image_constant_delta_omega = fftshift(fft(fftshift(signal_with_delta_omega)));

figure;
subplot(2, 2, 1); hold on;
plot(abs(signal_no_delta_omega));
plot(abs(signal_with_delta_omega));
hold off;
subplot(2, 2, 2); hold on;
plot(angle(signal_no_delta_omega));
plot(angle(signal_with_delta_omega));
hold off;
subplot(2, 2, 3); hold on;
plot(abs(image_no_delta_omega));
plot(abs(image_constant_delta_omega));
hold off;
subplot(2, 2, 4); hold on;
plot(angle(image_no_delta_omega));
plot(angle(image_constant_delta_omega));
hold off;

signal_with_delta_omega = simulate( ...
    M0, T1, T2, positions, delta_omega, ...
    5.1e-3, trajectory, G, delta_k, dwell_time);
image_constant_delta_omega = fftshift(fft(fftshift(signal_with_delta_omega)));

figure;
subplot(2, 2, 1); hold on;
plot(abs(signal_no_delta_omega));
plot(abs(signal_with_delta_omega));
hold off;
subplot(2, 2, 2); hold on;
plot(angle(signal_no_delta_omega));
plot(angle(signal_with_delta_omega));
hold off;
subplot(2, 2, 3); hold on;
plot(abs(image_no_delta_omega));
plot(abs(image_constant_delta_omega));
hold off;
subplot(2, 2, 4); hold on;
plot(angle(image_no_delta_omega));
plot(angle(image_constant_delta_omega));
hold off;

phase_shift = Simulator.gamma * 7 * 5e-6 * 1e-4;

signal_echo_1 = simulate( ...
    M0, T1, T2, positions, delta_omega, ...
    5e-3, trajectory, G, delta_k, dwell_time);
signal_echo_2 = simulate( ...
    M0, T1, T2, positions, delta_omega, ...
    5.1e-3, trajectory, G, delta_k, dwell_time);

image_echo_1 = fftshift(fft(fftshift(signal_echo_1)));
image_echo_2 = fftshift(fft(fftshift(signal_echo_2)));

B0_map_rad = angle(image_echo_2 .* conj(image_echo_1));
B0_map_ppm = 1e6 * B0_map_rad / (Simulator.gamma * 7 * 1e-4);

figure;
plot(B0_map_ppm);

function signal = simulate( ...
        M0, T1, T2, positions, delta_omega, ...
        TE, trajectory, G, delta_k, dwell_time)
    signal = zeros(1, length(trajectory));
    
    simulator = Simulator(M0, T1, T2, positions, delta_omega);
    
    simulator.pulse(pi/2);
    
    simulator.idle(TE-length(trajectory)/2*dwell_time);
    
    G_max = 25e-3; % mT/m
    tau_begin = abs(trajectory(1)/(simulator.gamma*G_max));
    G_begin = trajectory(1)/(simulator.gamma*tau_begin);
    simulator.gradient(G_begin, tau_begin);
    
    for i=1:length(trajectory)
        M = simulator.magnetization(1, :) + 1j*simulator.magnetization(2, :);
        
        k = 1+round((trajectory(i) - min(trajectory))/delta_k);
        signal(k) = sum(M);
        
        if i ~= length(trajectory)
            simulator.gradient(G(i), dwell_time);
        end
    end
end
