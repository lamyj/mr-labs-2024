Spectroscopy
############

I- Basics MR spectrum/signal Analysis
==================================

*Section authors:* Helene Ratiney <helene.ratiney@creatis.insa-lyon.fr>, Angeline Nemeth <angeline.nemeth@universite-paris-saclay.fr>


MR signal/spectrum display
--------------------------

The aim of this project is to get you to manipulate spectra and learn the first reflexes for displaying and quantifying spectra.
If you are using Python you will need to import the following modules for this session:

.. literalinclude:: TP_SRM.py
    :lines: 1-5

Change the name of the signal, spectrum and header each time you read a signal (for example sig1, ft1, header1, then sig2, ft2, header2, etc... ). *
Also, when using the solution code, change the path to your working directory accordingly.
1) load the Creatine signal from `<Cr.RAW>`__ using `<read_LCMRAW.m>`__ or `<tp_srm_utils.py>`__ and display it in time and frequency domain (x-axis in points) 
 
.. solution::
    
    .. tab:: Python
        
        .. literalinclude:: TP_SRM.py
            :lines: 10-29
    
    .. tab:: MATLAB
        
        .. literalinclude:: TP_SRM.m
            :lines: 2-27			
           			
2a.  The sampling time or dwell time for this signal was dt = 0.0004s, compute the sampling frequency and the frequency axis in Hz.Display the spectrum with the frequency axis in Hz. 

.. solution::
    
    .. tab:: Python
    
        .. literalinclude:: TP_SRM.py
            :lines: 34-36
			
    .. tab:: MATLAB
	
        .. literalinclude:: TP_SRM.m
            :lines: 30-56

2b. Measure the distance between the two peaks.
 			
.. solution::
    the frequency shift between the two peaks is around:  109 Hz. 
			
3a. Now, load the Creatine signal from `<Cr3.RAW>`__ and do the same as before but using dt=1.82999996E-04s. 

.. solution::
    
	.. tab:: Python
    
        .. literalinclude:: TP_SRM.py
            :lines: 58-83
			
    .. tab:: MATLAB
	
        .. literalinclude:: TP_SRM.m
            :lines: 58-93

3b. Measure again the distance between the two peaks.

.. solution::

    the frequency shift between the two peaks is around:  443 Hz.
	
4) Knowing that in the first case, the spectrum was acquired at 2.92 T, can you deduce at which field the second one was acquired?

.. solution::

		11.7T
	
5. An other way to display spectrum is to use ppm axe instead of Hz axe. As a reminder, the definition of ppm is : $\delta = \frac{\nu_0 - \nu_{0 ref}}{\nu_{0 ref}}$
Here, in the case of in vivo acquisition, the frequency $\nu_{0 ref}$ referes to the main carrier frequency present in biological tissues (the water). Normally, the water peak is located at 4.7 ppm.  The frequency $\nu_{0 ref}$ is saved in the header of the raw data (parameter "HZPPM" example : header1.HZPPM). 
Now, display the two spectra in ppm (think about shifting the spectrum by 4.7 ppm). 

.. solution::
    
    .. tab:: Python
        
        .. literalinclude:: TP_SRM.py
            :lines: 10-29
    
    .. tab:: MATLAB
        
        .. literalinclude:: TP_SRM.m
            :lines: 2-27

6. At this step, we are going to work with on other metabolite present in the brain : the lactate.
Load the file `<Lac.RAW>`__.
Display the spectrum, and identify the chemical shifts and multiplet based on the values published in table 1 publication Govindaraju et al. `DOI 10.1002/1099-1492(200005)13:3<129::AID-NBM619>3.0.CO <https://doi.org/10.1002/1099-1492(200005)13:3\<129::AID-NBM619>3.0.CO;2-V\> `

.. solution::

    .. tab:: Python

        .. literalinclude:: TP_SRM.py
            :lines: 99-113

    .. tab:: MATLAB

        .. literalinclude:: TP_SRM.m
            :lines: 114-137

Apodization
-----------
Stopping signal sampling before complete decay leads to truncation artifacts. One way to attenuate these artifacts is to apodize the signal by multiplying the temporal signal using an apodization function ( i.e multiplication with an exponential decay term.)

a) Apodize the Creatine signal (Cr.RAW)  with a damping factor of 5 Hz.

.. solution::

    .. tab:: Python

        .. literalinclude:: TP_SRM.py
            :lines: 123-130

    .. tab:: MATLAB

        .. literalinclude:: TP_SRM.m
            :lines: 140-144

b) Compare the two temporal signals (with and without apodization).

.. solution::

    .. tab:: Python

        .. literalinclude:: TP_SRM.py
            :lines: 128-130

    .. tab:: MATLAB

        .. literalinclude:: TP_SRM.m
            :lines: 145-151

c) Compare the two spectra of these signals.

.. solution::

    .. tab:: Python

        .. literalinclude:: TP_SRM.py
            :lines: 131-133

    .. tab:: MATLAB

        .. literalinclude:: TP_SRM.m
            :lines: 152-148

Frequency shift
---------------
Shifting the spectrum by a few Hz is useful to register different acquisition or to fit a model to an acquired spectrum. The frequency shift by $\delta f$ of a spectrum corresponds to a multiplication in the time domain by $exp(i2\pi\delta f)$

.. solution::

    .. tab:: Python

        .. literalinclude:: TP_SRM.py
        :lines: 136-143

    .. tab:: MATLAB

        .. literalinclude:: TP_SRM.m
        :lines: 179-185

Zero-order Phase
---------------
Sometimes, the acquired signal/spectrum is affected by a zero order phase. This corresponds to a multiplication of the signal with a fixed phase term $exp(i\phi_{0})$ , to see the effect of this term on the spectrum,
apply it to the Creatine signal and display the spectrum (the term can be multiplied either in the time domain
or in the frequency domain). Choose $\phi_{0}$ above 30°, remember that $\phi_0$ is expressed in rad.

.. solution::

    .. tab:: Python

        .. literalinclude:: TP_SRM.py
        :lines: 145-152

    .. tab:: MATLAB

        .. literalinclude:: TP_SRM.m
        :lines: 189-199

Create your signal (weighted sum of metabolites)
-----------------------------------------------

Load all the available metabolite signal (`<PCho.RAW>`__, `<NAA.RAW>`__) and create a signal as a weighted sum of metabolites, such as $sig(t) =\sum a_k metabolite_k(t) exp(-dt) +noise$ where noise is a gaussian noise with a standard deviation between 0 and 1.5. And write it in a .RAW format using `<write_LCMRAW.m>`__ or `<tp_srm_utils.py>`__.
You can create several signals, and write them with different names.

.. solution::

    .. tab:: Python

        .. literalinclude:: TP_SRM.py
        :lines: 154-189

    .. tab:: MATLAB

        .. literalinclude:: TP_SRM.m
        :lines: 201-236

Quantify your signal using LCModel
-----------------------------------------------
LCmodel is a quantification method . You can load your signal an the VIP plateform and launch the quantification. It can be done either on the VIP web site (<https://docs.google.com/document/d/1e3E7ei0tkW7vdcN9ZNR-RA2HYmF1RY3XQU3zR7AdNic/edit?usp=sharing>), or using the launch_lcmodel.ipynb

II- Magnetic Resonance Spectroscopie Imaging space handling
===========================================================

Introduction
----------------

The aim of this TP is to simulate the MRSI data and navigate in the different domain.

In the first step, you will generate a virtual phantom of a head with different tissues/compartments :

- White matter (WM)

- Grey matter (GM)

- Cerebrospinal fluid (CSF)

- Subcutaneous lipids (Lip)

For each brain tissue, you will assign different metabolite concentrations from the set of Creatine (Cr), Phosphocholine (PCho), Lactate (Lac), N-acetylaspartate (NAA) as described in the following table.

===========  ====  ====
Metabolite   WM    GM
===========  ====  ====
Cr           8     8
Pcho         3     2
Lac          10    10
NAA          1     1
===========  ====  ====

Once the virtual phantom is generated in a "high resolution" version, you will undersampling the image matrix and see the results.

Virtual phantom generation
----------------------------

1) Load the different mask (`<mask_WM.mat>`__, `<mask_GM.mat>`__, `<mask_CSF.mat>`__, `<mask_Lip.mat>`__) and visualise them.

.. solution::

    .. tab:: Python

        .. literalinclude:: MRSI_simu_TP.py
            :lines: 6-31

    .. tab:: MATLAB

        .. literalinclude:: MRSI_simu_TP.m
            :lines: 3-20

2) For the GW and WM, generate the spectroscopic signal on each point of the mask with the concentrations of metabolite described above. Use a dwell time of 0.0004 second and an apodisation of 10 Hz.

.. solution::

    .. tab:: Python

        .. literalinclude:: MRSI_simu_TP.py
            :lines: 35-62

    .. tab:: MATLAB

        .. literalinclude:: MRSI_simu_TP.m
            :lines: 23-51
3)Do the same for the subcutaneous lipids by using the `<generateLipids.m>`__ / `<tp_srm_utils.py>`__ function to generate the signal from lipid.

.. solution::

    .. tab:: Python

        .. literalinclude:: MRSI_simu_TP.py
            :lines: 64-68

    .. tab:: MATLAB

        .. literalinclude:: MRSI_simu_TP.m
            :lines: 52-55

4) Combinate the images of the different tissues to have an unique image and add gaussian noise with a sigma of 0.5.

.. solution::

    .. tab:: Python

        .. literalinclude:: MRSI_simu_TP.py
            :lines: 72-74

    .. tab:: MATLAB

        .. literalinclude:: MRSI_simu_TP.m
            :lines: 64-66

Manipulation of k-t images
----------------------------
At this point, you have generated a matrix whose first two dimensions refer to the spatial domain and the last is the temporal evolution of the spectroscopic signal.

5)Transform the matrix created for the first two dimensions to represent the k-space instead of the image domain.

.. solution::

    .. tab:: Python

        .. literalinclude:: MRSI_simu_TP.py
            :lines: 76-84

    .. tab:: MATLAB

        .. literalinclude:: MRSI_simu_TP.m
            :lines: 69-74

6) Find where the maximum signal is located in the k-space.

.. solution::

    .. tab:: Python

        .. literalinclude:: MRSI_simu_TP.py
            :lines: 87-96

    .. tab:: MATLAB

        .. literalinclude:: MRSI_simu_TP.m
            :lines: 80-81

7)What does the spectrum associated with this point correspond to?

.. tab:: Python

	.. literalinclude:: MRSI_simu_TP.py
		:lines: 98-102

.. tab:: MATLAB

	.. literalinclude:: MRSI_simu_TP.m
		:lines: 84-85

.. solution::
	The signal maximum is at the center of Fourier space (therefore without spatial encoding). The spectrum displayed corresponds to the entire signal coming from the entire phantom.

Truncation
-------------
8)Reduce the matrix size by a factor 16 by removing the high frequency from the k-space.

.. solution::

    .. tab:: Python

        .. literalinclude:: MRSI_simu_TP.py
            :lines: 105-112

    .. tab:: MATLAB

        .. literalinclude:: MRSI_simu_TP.m
            :lines: 90

9)Return to the domain image for the first two dimensions of the matrix.

.. solution::

    .. tab:: Python

        .. literalinclude:: MRSI_simu_TP.py
            :lines: 116-118

    .. tab:: MATLAB

        .. literalinclude:: MRSI_simu_TP.m
            :lines: 94-96


10) Transform the last temporal dimension into the frequency domain.

.. solution::

    .. tab:: Python

        .. literalinclude:: MRSI_simu_TP.py
            :lines: 125-131

    .. tab:: MATLAB

        .. literalinclude:: MRSI_simu_TP.m
            :lines: 104-110

11) Display two spectra in the brain tissue from adjacent voxels (X1 = 19 and Y1 = 10, X2 = 19 and Y2 = 11).
What do you observe? Try to explain the phenomen. How could we get rid of it?

.. solution::

    .. tab:: Python

        .. literalinclude:: MRSI_simu_TP.py
            :lines: 133-140

    .. tab:: MATLAB

        .. literalinclude:: MRSI_simu_TP.m
            :lines: 111-112
