import matplotlib.pyplot
import numpy

from simulator import Simulator

import style

def main():
    # Shape and positions of the objects
    radius = 0.02 # m
    centers = [-0.05, 0., 0.05] # m
    
    # Geometry of the image
    fov = 0.15 # m
    voxel_size = 1e-3 # m
    definition = int(fov/voxel_size) # unitless
    
    length = int(2*radius/voxel_size) # number of voxels in each object
    positions = numpy.concatenate([
        voxel_size*numpy.arange(-length/2, length/2)+c for c in centers]) # m
    M0 = numpy.concatenate([length*[1.], length*[0.7], length*[0.5]]) # arbitrary units
    T1 = numpy.concatenate([length*[1.], length*[0.7], length*[2.0]]) # s
    T2 = numpy.concatenate([length*[10e-3], length*[50e-3], length*[100e-3]]) # s
    
    k_max = 0.5 * 2*numpy.pi/voxel_size # rad/m
    k_definition = definition # unitless
    delta_k = 2*k_max/(k_definition-1) # rad/m
    trajectory = numpy.linspace(-k_max, k_max, k_definition) # rad/m
    
    dwell_time = 10e-6 # s
    G = numpy.diff(trajectory, axis=0)/(Simulator.gamma*dwell_time) # T/m
    
    signals = {}
    
    delta_omega = numpy.zeros(len(positions))
    signals[r"$\Delta B_0=0$"] = simulate(
        M0, T1, T2, positions, delta_omega,
        5e-3, trajectory, G, delta_k, dwell_time)
    
    epsilon = Simulator.gamma_bar * 7 * 5e-6 # Hz
    print(f"B0 frequency offset: {epsilon:.2f} Hz")
    delta_omega = numpy.full(len(positions), epsilon)
    
    signals[r"$\Delta B_0 \ne 0$"] = simulate(
        M0, T1, T2, positions, delta_omega,
        5e-3, trajectory, G, delta_k, dwell_time)
    images = {
        k: numpy.fft.fftshift(numpy.fft.fft(numpy.fft.fftshift(v)))
        for k,v in signals.items() }
    
    figure, plots = matplotlib.pyplot.subplots(
        2, 2, layout="tight", figsize=(4,4))
    for name, signal in signals.items():
        plots[0,0].plot(numpy.abs(signal), label=name)
        plots[0,1].plot(numpy.angle(signal))
        plots[1,0].plot(numpy.abs(images[name]))
        plots[1,1].plot(numpy.angle(images[name]))
    plots[0,0].set(ylabel="Signal magnitude (a.u.)")
    plots[0,1].set(ylabel="Signal phase (rad)")
    plots[1,0].set(ylabel="Image magnitude (a.u.)")
    plots[1,1].set(ylabel="Image phase (rad)")
    plots[0, 0].legend()
    figure.savefig("artifacts_B0_constant.png")
    
    delta_omega = numpy.concatenate([
        length*[-2*epsilon], length*[epsilon], length*[2*epsilon]])
    
    signals[r"$\Delta B_0 \ne 0$"] = simulate(
        M0, T1, T2, positions, delta_omega,
        5e-3, trajectory, G, delta_k, dwell_time)
    images = {
        k: numpy.fft.fftshift(numpy.fft.fft(numpy.fft.fftshift(v)))
        for k,v in signals.items() }
    
    figure, plots = matplotlib.pyplot.subplots(
        2, 2, layout="tight", figsize=(4,4))
    for name, signal in signals.items():
        plots[0,0].plot(numpy.abs(signal), label=name)
        plots[0,1].plot(numpy.angle(signal))
        plots[1,0].plot(numpy.abs(images[name]))
        plots[1,1].plot(numpy.angle(images[name]))
    plots[0,0].set(ylabel="Signal magnitude (a.u.)")
    plots[0,1].set(ylabel="Signal phase (rad)")
    plots[1,0].set(ylabel="Image magnitude (a.u.)")
    plots[1,1].set(ylabel="Image phase (rad)")
    plots[0, 0].legend()
    figure.savefig("artifacts_B0_by_object.png")
    
    signals[r"$\Delta B_0 \ne 0$"] = simulate(
        M0, T1, T2, positions, delta_omega,
        5.1e-3, trajectory, G, delta_k, dwell_time)
    images = {
        k: numpy.fft.fftshift(numpy.fft.fft(numpy.fft.fftshift(v)))
        for k,v in signals.items() }
    
    figure, plots = matplotlib.pyplot.subplots(
        2, 2, layout="tight", figsize=(4,4))
    for name, signal in signals.items():
        plots[0,0].plot(numpy.abs(signal), label=name)
        plots[0,1].plot(numpy.angle(signal))
        plots[1,0].plot(numpy.abs(images[name]))
        plots[1,1].plot(numpy.angle(images[name]))
    plots[0,0].set(ylabel="Signal magnitude (a.u.)")
    plots[0,1].set(ylabel="Signal phase (rad)")
    plots[1,0].set(ylabel="Image magnitude (a.u.)")
    plots[1,1].set(ylabel="Image phase (rad)")
    plots[0, 0].legend()
    figure.savefig("artifacts_B0_long.png")
    
    phase_shift = Simulator.gamma * 7 * 5e-6 * 1e-4
    print(
        "Phase shift with ΔB₀ of 5 ppm at 7 T, ΔTE=0.1 ms: "
        f"{phase_shift:.2f} rad")
    
    signals[r"TE=5 ms"] = simulate(
        M0, T1, T2, positions, delta_omega,
        5e-3, trajectory, G, delta_k, dwell_time)
    signals[r"TE=5.1 ms"] = simulate(
        M0, T1, T2, positions, delta_omega,
        5.1e-3, trajectory, G, delta_k, dwell_time)
    images = {
        k: numpy.fft.fftshift(numpy.fft.fft(numpy.fft.fftshift(v)))
        for k,v in signals.items() }
    B0_map_rad = numpy.angle(images["TE=5.1 ms"] * images["TE=5 ms"].conj())
    B0_map_ppm = 1e6 * B0_map_rad / (Simulator.gamma * 7 * 1e-4)
    
    figure, plot = matplotlib.pyplot.subplots(layout="tight", figsize=(2,2))
    plot.plot(B0_map_ppm)
    plot.set(xlabel="Position (voxels)", ylabel="$\Delta B_0$ (ppm)")
    figure.savefig("artifacts_B0_map.png")

def simulate(
        M0, T1, T2, positions, delta_omega,
        TE, trajectory, G, delta_k, dwell_time):
    signal = numpy.zeros(len(trajectory), complex)
    
    simulator = Simulator(M0, T1, T2, positions, delta_omega)
    
    simulator.pulse(numpy.pi/2)
    
    simulator.idle(TE-len(trajectory)/2*dwell_time)
    
    G_max = 25e-3 # mT/m
    tau_begin = numpy.abs(trajectory[0])/(simulator.gamma*G_max)
    G_begin = trajectory[0]/(simulator.gamma*tau_begin)
    simulator.gradient(G_begin, tau_begin)
    
    for i in range(len(trajectory)):
        M = simulator.magnetization[:, 0] + 1j*simulator.magnetization[:, 1]
        
        k = numpy.round((trajectory[i] - trajectory.min())/delta_k).astype(int)
        signal[k] = M.sum()
        
        if i != len(trajectory)-1:
            simulator.gradient(G[i], dwell_time)
    
    return signal

if __name__ == "__main__":
    main()
