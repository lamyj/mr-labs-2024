import numpy as np
import matplotlib
from tp_srm_utils import SRM_utils as tpu
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt


# Read the signal written in Cr.Raw

nom= "D:\\work\\LCModel\\.lcmodel\\tp_arcachon\\Cr.RAW"
header1, sig1 = tpu.read_LCMRAW(nom)

# Plot time domain and Frequency Domain, with x-axis in point
indices= np.arange(len(sig1))
fig2, ax2 = plt.subplots(2, 1)
ax2[0].plot(indices, np.real(sig1), 'gray')
ax2[0].set_title('Signal - Partie Réelle')
ax2[0].set_xlabel('Temps')
ax2[0].set_ylabel('Amplitude')
ax2[0].grid(True)
plt.show()

ft1 = tpu.fft_signal(signal=np.conj(sig1))
ax2[1].plot(indices, np.real(ft1), 'gray')
ax2[1].set_title('Spectre - Partie Réelle')
ax2[1].set_xlabel('Frequence')
ax2[1].set_ylabel('Amplitude')
ax2[1].grid(True)
plt.show()

###
# the sampling time also call dwell time is 0.4ms ,
# display the signal with x-axis in s for time domain, x-axis in Hz for frequency domain representation
dt=0.0004
# Compute frequency axis in Hz
freq_values1 =np.fft.fftshift(np.fft.fftfreq(len(sig1), dt))
#Compute time_axis in s
time_axis1 = np.arange(0, dt * len(sig1), dt)
#Displau
fig2, ax2 = plt.subplots(2, 1)
ax2[0].plot(time_axis1, np.real(sig1), 'gray')
ax2[0].set_title('Signal - Partie Réelle')
ax2[0].set_xlabel('Temps')
ax2[0].set_ylabel('Amplitude')
ax2[0].grid(True)
plt.show()

ft = tpu.fft_signal(signal=np.conj(sig1))
ax2[1].plot(freq_values, np.abs(ft), 'gray')
ax2[1].set_title('Spectre - Partie Réelle')
ax2[1].set_xlabel('Frequence')
ax2[1].set_ylabel('Amplitude')
ax2[1].grid(True)
plt.show()

# Read the signal written in Cr2.Raw

nom= "D:\\work\\LCModel\\.lcmodel\\tp_arcachon\\Cr2.RAW"
header2, sig2 = tpu.read_LCMRAW(nom)
# the sampling time also call dwell time is 0.4ms ,
# display the signal with x-axis in s for time domain, x-axis in Hz for frequency domain representation
dt=0.00018299
# Compute frequency axis in Hz
freq_values2 =np.fft.fftshift(np.fft.fftfreq(len(sig2), dt))
#1Compute time_axis in s
time_axis2 = np.arange(0, dt * len(sig2), dt)
#Displau
fig2, ax2 = plt.subplots(2, 1)
ax2[0].plot(time_axis2, np.real(sig2), 'gray')
ax2[0].set_title('Signal - Partie Réelle')
ax2[0].set_xlabel('Temps')
ax2[0].set_ylabel('Amplitude')
ax2[0].grid(True)
plt.show()


ft2 = tpu.fft_signal(signal=np.conj(sig2))
ax2[1].plot(freq_values, np.abs(ft), 'gray')
ax2[1].set_title('Spectre - Partie Réelle')
ax2[1].set_xlabel('Frequence')
ax2[1].set_ylabel('Amplitude')
ax2[1].grid(True)
plt.show()


# now display the spectrum with the ppm convention (you will need to retrieve the carrier frequency from the header)
# Dispplay in  ppm
plt.figure(3)
# you will
cfreq1= float(list(header1.values())[1])
ppm_values1 =freq_values1/cfreq1+4.7
plt.plot(ppm_values1,np.real(ft1), 'blue')
plt.xlim([0,5.5])
plt.gca().invert_xaxis()
plt.figure(3)
# you will
cfreq2= float(list(header2.values())[1])
print(cfreq)
ppm_values2 =freq_values2/cfreq2+4.7
plt.plot(ppm_values2,np.real(ft2), 'blue')
plt.xlim([0,5.5])
plt.gca().invert_xaxis()
# Redo the same thing with the Lac , reteive the ppm value of the different chemical groups, measure on the doublet (spectrum in absolutevalue)   Jcoupling value
## Faire la meme chose avec Lac

nom= "D:\\work\\LCModel\\.lcmodel\\tp_arcachon\\Lac.RAW"
header3, sig3 = tpu.read_LCMRAW(nom)
# display the signal with x-axis in s for time domain, x-axis in Hz for frequency domain representation
dt=0.0004
# Compute frequency axis in Hz
freq_values3 =np.fft.fftshift(np.fft.fftfreq(len(sig3), dt))
plt.figure(4)
ft3 = tpu.fft_signal(signal=np.conj(sig3))
# you will
cfreq3= float(list(header3.values())[1])
print(cfreq3)
ppm_values3 =freq_values3/cfreq3+4.7
plt.plot(ppm_values3,np.real(ft3), 'blue')
plt.xlim([0,5.5])
plt.gca().invert_xaxis()


## Apodization
apo=5;
ap_func=np.exp(-apo*time_axis1);
sig_apo = np.squeeze(sig1)* np.array(ap_func)
ft_apo = tpu.fft_signal(signal=np.conj(sig_apo))
plt.figure(5)
plt.plot(time_axis1,np.real(sig1),'black')
plt.plot(time_axis1,np.real(sig_apo),'blue')
plt.figure(7)
plt.plot(ppm_values1,np.real(ft1),'black')
plt.plot(ppm_values1,np.real(ft_apo),'blue')


## frequency shift
delta_f=10;
shift_func=np.exp(1j*2*np.pi*delta_f*time_axis1)
sig_shift= np.squeeze(sig1)* np.array(shift_func)
ft_shift=tpu.fft_signal(ignal=np.conj(sig_shift))
plt.figure(4)
plt.plot(ppm_values1,np.real(ft1),'black')
plt.plot(ppm_values1,np.real(sig_shift),'blue')

## Déphasage
phi0=50*np.pi/180
phi0_func=np.exp(1j*phi0)
sig_phi0= np.squeeze(sig1)* phi0_func
ft_phi0 = tpu.fft_signal(signal=np.conj(sig_phi0))
plt.figure(4)
plt.plot(ppm_values1,np.real(ft1),'black')
plt.plot(ppm_values1,np.real(ft_phi0),'blue')

# Sommer 4 métabolites avec amplitude choisie
Ampl=[1,8,10,3]
met1 = "D:\\work\\LCModel\\.lcmodel\\tp_arcachon\\Lac.RAW"
met2 = "D:\\work\\LCModel\\.lcmodel\\tp_arcachon\\Cr.RAW"
met3= "D:\\work\\LCModel\\.lcmodel\\tp_arcachon\\NAA.RAW"
met4="D:\\work\\LCModel\\.lcmodel\\tp_arcachon\\PCho.RAW"
liste_fichiers= [met1, met2,met3,met4]
signals=[]
for fichier in liste_fichiers:
        header, signal = tpu.read_LCMRAW(fichier)
        signals.append(signal)


sigAll = np.dot(np.array(signals).T, Ampl).T
sigAll_apo = np.squeeze(sigAll)* np.array(ap_func)
sigAllf= np.squeeze(sigAll_apo)* phi0_func

# Add noise
sigma = 0.5  # Vous pouvez ajuster cette valeur selon votre besoin

# Generate a gaussian noise for the real part and imageinary part
noise_real = np.random.normal(0, sigma, len(sigAllf))
noise_imaginary = np.random.normal(0, sigma, len(sigAllf))

#Sum the noise and signal
Final=sigAllf+noise_real+1j*noise_imaginary


plt.figure(3)
ft = tpu.fft_signal(signal=np.conj(Final))
cfreq= float(list(header.values())[1])
print(cfreq)
ppm_values =freq_values/cfreq+4.7
plt.plot(ppm_values,np.real(ft), 'blue')
plt.xlim([0,5.5])
plt.gca().invert_xaxis()

#Write your spectrum (you can do several version)
tpu.write_LCMRAW(Final, TE=30, HZPPPM=cfreq, SEQ='STEAM', ID=None, FMTDAT='7.3f', VOLUME=1, TRAMP=1, outputfilename='test1.RAW')

#ax[0].set_xlim([0, 5.5])
#ax2[1].plot(my_mrs_signal.frequency_axis_ppm(), np.real(my_mrs_signal.spectrum()), 'red')

#ax2[0].invert_xaxis()
#ax2[1].invert_xaxis()
