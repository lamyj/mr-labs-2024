import scipy.io
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from tp_srm_utils import SRM_utils as tpu
# Load the different masks
mask_CSF = scipy.io.loadmat('mask_CSF.mat')['mask_CSF']
mask_GM = scipy.io.loadmat('mask_GM.mat')['mask_GM']
mask_Lip = scipy.io.loadmat('mask_Lip.mat')['mask_Lip']
mask_WM = scipy.io.loadmat('mask_WM.mat')['mask_WM']

# Visualize the masks
plt.figure(figsize=(10, 8))

plt.subplot(2, 2, 1)
plt.imshow(mask_CSF, cmap=cm.gray)
plt.title('mask_CSF')

plt.subplot(2, 2, 2)
plt.imshow(mask_WM, cmap=cm.gray)
plt.title('mask_WM')

plt.subplot(2, 2, 3)
plt.imshow(mask_GM, cmap=cm.gray)
plt.title('mask_GM')

plt.subplot(2, 2, 4)
plt.imshow(mask_Lip, cmap=cm.gray)
plt.title('mask_Lip')

plt.show()

# Create your High resolution MRSI grid Space (Image, t)

# Load signals
header,sigPCr = tpu.read_LCMRAW("D:\\work\\LCModel\\.lcmodel\\tp_arcachon\\Cr.RAW")
header,sigPCho = tpu.read_LCMRAW("D:\\work\\LCModel\\.lcmodel\\tp_arcachon\\Pcho.RAW")
header,sigNAA = tpu.read_LCMRAW("D:\\work\\LCModel\\.lcmodel\\tp_arcachon\\NAA.RAW")
header,sigLac = tpu.read_LCMRAW("D:\\work\\LCModel\\.lcmodel\\tp_arcachon\\Lac.RAW")

Wm_prop = [8, 3, 10, 1]
Gm_prop = [8, 2, 10, 1]
apo = 10
N = len(sigPCr)
dt = 0.0004
tt = np.arange(0, N*dt, dt)

# generate a spectropic signal with correct concentrations of metabolites
sigwm = np.dot(np.array([sigPCr, sigPCho, sigNAA, sigLac]).T, Wm_prop)
sigwm *= np.exp(-apo * tt)
sigm = np.dot(np.array([sigPCr, sigPCho, sigNAA, sigLac]).T, Gm_prop)
sigm *= np.exp(-apo * tt)
# copy with signal each point of the mask
# first, create an image with the same dimension of the mask
# we find the spectroscopic signal in all the voxels
# gmgrid
gmgrid = np.tile(np.reshape(sigm, (1, 1, N)), (mask_GM.shape[0], mask_GM.shape[1], 1))
# wmgrid
wmgrid = np.tile(np.reshape(sigwm, (1, 1, N)), (mask_WM.shape[0], mask_WM.shape[1], 1))
# create MRSI grid per mask high resolution
csiGM = np.repeat(mask_GM[ :, :,np.newaxis], 2048, axis=2) * gmgrid
csiWM = np.repeat(mask_WM[ :, :,np.newaxis], 2048, axis=2) * wmgrid

# lipid signal
lip = tpu.generateLipids(dt, float(header['HZPPM']), 20, N)
# lipgrid
lipgrid = np.tile(np.reshape(lip, (1, 1, N)), (mask_GM.shape[0], mask_GM.shape[1], 1))
csilip = np.repeat(mask_Lip[ :, :,np.newaxis], 2048, axis=2) * lipgrid


# csi_final = csiGM + csiWM + noise
sigma = 0.5
noise = np.random.randn(N) *sigma # Noise array
csi_final = csilip + csiGM + csiWM + noise

# Get the dimensions of mask_GM
N1, N2 = mask_GM.shape

# Initialize array for kcsi
kcsi = np.zeros((N1, N2, N), dtype=np.complex128)

# Go to the k,t space
for k in range(N):
    kcsi[:, :, k] =np.fft.fftshift(np.fft.fft2(csi_final[:, :, k]))

# Rechercher le voxel de maximum intensité sur le premier point temporel de kcsi
mx = np.argmax(np.abs(kcsi[:, :, 0]))

# Affichage de l'image réelle du premier plan temporel de kcsi
plt.imshow(np.squeeze(np.real(kcsi[:, :, 0])), cmap='gray')
plt.colorbar()
plt.show()

# Trouver les coordonnées du maximum
I, J = np.unravel_index(np.argmax(np.abs(kcsi[:, :, 0])), kcsi[:, :, 0].shape)

# Plot du spectre
spectrum = np.fft.fftshift(np.fft.fft(np.squeeze(kcsi[I, J, :])))
plt.plot(np.real(spectrum))
plt.xlabel('Fréquence')
plt.ylabel('Partie réelle du spectre')
plt.show()

# Détermination des indices de début et de fin pour la troncature
fac=16
start_i = max(I - N1//fac, 0)
end_i = min(I + N1//fac, N1)
start_j = max(J - N2//fac, 0)
end_j = min(J + N2//fac, N2)

# Troncature de kcsi
kcsitr = kcsi[start_i:end_i, start_j:end_j, :]


# Retour à l'espace image
csilr2 = np.zeros_like(kcsitr, dtype=np.complex128)
for k in range(N):
    csilr2[:, :, k] = np.fft.ifft2(np.fft.ifftshift(kcsitr[:, :, k]))

# Affichage de l'image réelle
plt.imshow(np.squeeze(np.real(csilr2[:, :, 0])), cmap='gray')
plt.colorbar()
plt.show()

# Transformation de Fourier inverse
Ni1 = kcsitr.shape[0]
Ni2 = kcsitr.shape[1]
csift2 = np.zeros((Ni1, Ni2, N), dtype=np.complex128)
for k in range(Ni1):
    for l in range(Ni2):
        csift2[k, l, :] = np.fft.fftshift(np.fft.fft(np.squeeze(csilr2[k, l, :])))

# Affichage de la partie réelle
plt.imshow(np.real(np.squeeze(csift2[18, 9, :])), cmap='gray')
plt.colorbar()
plt.show()
# Affichage de la partie réelle
plt.imshow(np.real(np.squeeze(csift2[18, 10, :])), cmap='gray')
plt.colorbar()
plt.show()
        
        
# Affichage de la partie réelle
plt.imshow(np.real(np.squeeze(csift2[:, :, 1369])), cmap='gray')
plt.colorbar()
plt.show()

# Affichage de la phase
plt.imshow(np.angle(np.squeeze(csift2[:, :, 1369])), cmap='gray')
plt.colorbar()
plt.show()