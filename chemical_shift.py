import matplotlib.pyplot
import numpy

from simulator import Simulator

import style

def main():
    # Shape and positions of the objects
    radius = 0.025 # m
    centers = [-0.05, 0., 0.05] # m

    # Geometry of the image
    fov = 0.15 # m
    voxel_size = 1e-3 # m
    definition = int(fov/voxel_size) # voxels

    length = int(2*radius/voxel_size) # number of voxels in each object
    positions = numpy.concatenate([
        voxel_size*numpy.arange(-length/2, length/2)+c for c in centers]) # m
    M0 = numpy.concatenate([length*[1], length*[1], length*[1]]) # arbitrary units
    T1 = numpy.concatenate([length*[1.], length*[1], length*[1]]) # s
    T2 = numpy.concatenate([length*[100e-3], length*[100e-3], length*[100e-3]]) # s
    
    delta_omega_fat = Simulator.gamma_bar * 1.5 * 3.5e-6 # Hz
    delta_omega = numpy.concatenate([
        length*[delta_omega_fat], length*[0], length*[delta_omega_fat]])
    
    k_max = 0.5 * 2*numpy.pi/voxel_size # rad/m
    k_definition = definition # unitless
    delta_k = 2*k_max/(k_definition-1) # rad/m
    trajectory = numpy.linspace(-k_max, k_max, k_definition) # rad/m
    
    signals = {}
    
    dwell_time = 50e-6 # s
    G = numpy.diff(trajectory, axis=0)/(Simulator.gamma*dwell_time) # T/m
    signals["Dwell time = 50 µs"] = simulate(
        M0, T1, T2, positions, delta_omega,
        5e-3, trajectory, G, delta_k, dwell_time)
    
    dwell_time = 200e-6 # s
    G = numpy.diff(trajectory, axis=0)/(Simulator.gamma*dwell_time) # T/m
    signals["Dwell time = 200 µs"] = simulate(
        M0, T1, T2, positions, delta_omega,
        5e-3, trajectory, G, delta_k, dwell_time)
    
    images = {
        name: numpy.fft.fftshift(numpy.fft.fft(numpy.fft.fftshift(signal)))
        for name, signal in signals.items()}
    
    figure, plots = matplotlib.pyplot.subplots(
        1, 2, sharex=True, sharey=True, layout="tight", figsize=(4, 2.2))
    for (name, image), plot in zip(images.items(), plots):
        plot.plot(numpy.abs(image))
        plot.set(xlabel="Position (voxel)", title=name)
    plots[0].set(ylabel="$M_\perp/M_0$ (unitless)")
    figure.savefig("chemical_shift.png")
    
    shift = delta_omega_fat * len(trajectory)*50e-6
    print(f"Shift with a dwell time of 50 µs: {shift:.2f} pixels")
    
    shift = delta_omega_fat * len(trajectory)*200e-6
    print(f"Shift with a dwell time of 200 µs: {shift:.2f} pixels")
    
    shift = 0.5
    readout_time = shift/delta_omega_fat # s
    print(f"Readout time for a shift of 0.5 pixel: {1e3*readout_time:.2f} ms")
    dwell_time = readout_time/len(trajectory) # s
    print(f"Dwell time for a shift of 0.5 pixel: {1e6*dwell_time:.2f} µs")
    
    G = 2*k_max / Simulator.gamma / readout_time # T/m
    print(f"Gradient time for a shift of 0.5 pixel: {1e3*G:.2f} mT/m")

def simulate(
        M0, T1, T2, positions, delta_omega,
        TE, trajectory, G, delta_k, dwell_time):
    signal = numpy.zeros(len(trajectory), complex)
    
    simulator = Simulator(M0, T1, T2, positions, delta_omega)
    
    simulator.pulse(numpy.pi/2)
    
    simulator.idle(TE-len(trajectory)/2*dwell_time)
    
    G_max = 25e-3 # mT/m
    tau_begin = numpy.abs(trajectory[0])/(simulator.gamma*G_max)
    G_begin = trajectory[0]/(simulator.gamma*tau_begin)
    simulator.gradient(G_begin, tau_begin)
    
    for i in range(len(trajectory)):
        M = simulator.magnetization[:, 0] + 1j*simulator.magnetization[:, 1]
        
        k = numpy.round((trajectory[i] - trajectory.min())/delta_k).astype(int)
        signal[k] = M.sum()
        
        if i != len(trajectory)-1:
            simulator.gradient(G[i], dwell_time)
    
    return signal

if __name__ == "__main__":
    main()
