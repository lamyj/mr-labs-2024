Lab Sessions
============

These lab sessions are organized in different independent units. Each of them will require either Python (with Numpy, Scipy and Matplotlib) or MATLAB (≥ R2020b).

So that you can progress evenly, the solutions to all questions are given in the text, but are hidden unless you click on them.

.. solution::
    
    Don't read unless you're stuck!

For obvious pedagogical reasons, you should try to solve each exercise before looking at the solution.

The full code for all sessions is also provided for reference, both in Python and MATLAB

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :hidden:
   
   Introduction <self>
   bloch
   spatial_encoding
   tp_srm
   artifacts
   quantification
   solutions
