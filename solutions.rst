Full Solutions
==============

Bloch Equation: `Python <saturation_recovery.py>`__ / `MATLAB <saturation_recovery.m>`__

Spatial encoding

- 1D: `Python <spatial_encoding_1d.py>`__ / `MATLAB <spatial_encoding_1d.m>`__
- 2D: `Python <spatial_encoding_zig_zag.py>`__ / `MATLAB <spatial_encoding_zig_zag.m>`__

Artifacts

- B₀ Inhomogeneity: `Python <artifacts_B0.py>`__ / `MATLAB <artifacts_B0.m>`__
- B₁ Inhomogeneity: `Python <artifacts_B1.py>`__ / `MATLAB <artifacts_B1.m>`__
- Chemical Shift: `Python <chemical_shift.py>`__ / `MATLAB <chemical_shift.m>`__

Quantification

- Quantification of T₁: `Python <quantification_T1.py>`__
