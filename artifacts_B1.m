radius = 0.02; % m
centers = [-0.05, 0., 0.05]; % m

fov = 0.15; % m
voxel_size = 1e-3; % m
definition = fov/voxel_size; % voxels

object_length = 2*radius/voxel_size; % number of voxels in each object
positions = [];
for i=1:length(centers)
    positions = [ ...
        positions, voxel_size * (-object_length/2:object_length/2-1)+centers(i)];
end

M0 = [ ...
    repelem(1, object_length), ...
    repelem(0.7, object_length), ...
    repelem(0.5, object_length)]; % arbitrary units
T1 = [ ...
    repelem(1, object_length), ...
    repelem(0.7, object_length), ...
    repelem(2.0, object_length)]; % s
T2 = [ ...
    repelem(10e-3, object_length), ...
    repelem(50e-3, object_length), ...
    repelem(100e-3, object_length)]; % s

k_definition = definition; % unitless
k_max = 0.5 * 2*pi/voxel_size; % rad/m
delta_k = 2*k_max/(k_definition-1); % rad/m
trajectory = linspace(-k_max, k_max, k_definition);

dwell_time = 10e-6 ; % s
G = diff(trajectory)/(Simulator.gamma*dwell_time) ; % T/m

relative_B1 = 1;
signal_no_delta_B1 = simulate( ...
    M0, T1, T2, positions, relative_B1, ...
    deg2rad(20), trajectory, G, delta_k, dwell_time);
image_no_delta_B1 = fftshift(fft(fftshift(signal_no_delta_B1)));

relative_B1 = 0.9;
signal_with_delta_B1 = simulate( ...
    M0, T1, T2, positions, relative_B1, ...
    deg2rad(20), trajectory, G, delta_k, dwell_time);
image_with_delta_B1 = fftshift(fft(fftshift(signal_with_delta_B1)));

figure;
hold on;
plot(abs(image_no_delta_B1));
plot(abs(image_with_delta_B1));
hold off;

relative_B1 = [ ...
    repelem(0.7, object_length), ...
    repelem(1.1, object_length), ...
    repelem(1.3, object_length)];
signal_with_delta_B1 = simulate( ...
    M0, T1, T2, positions, relative_B1, ...
    deg2rad(20), trajectory, G, delta_k, dwell_time);
image_with_delta_B1 = fftshift(fft(fftshift(signal_with_delta_B1)));

figure;
hold on;
plot(abs(image_no_delta_B1));
plot(abs(image_with_delta_B1));
hold off;

signal_with_delta_B1 = simulate( ...
    M0, T1, T2, positions, relative_B1, ...
    deg2rad(40), trajectory, G, delta_k, dwell_time);
image_with_delta_B1 = fftshift(fft(fftshift(signal_with_delta_B1)));

figure;
hold on;
plot(abs(image_no_delta_B1));
plot(abs(image_with_delta_B1));
hold off;

signal_simple_angle = simulate( ...
    M0, T1, T2, positions, relative_B1, ...
    deg2rad(20), trajectory, G, delta_k, dwell_time);
image_simple_angle = fftshift(fft(fftshift(signal_simple_angle)));

signal_double_angle = simulate( ...
    M0, T1, T2, positions, relative_B1, ...
    deg2rad(40), trajectory, G, delta_k, dwell_time);
image_double_angle = fftshift(fft(fftshift(signal_double_angle)));

ratio = abs(image_double_angle) ./ abs(image_simple_angle);
actual_flip_angle = acos(ratio/2);
relative_B1 = actual_flip_angle ./ deg2rad(20);

figure;
plot(relative_B1);

function signal = simulate( ...
        M0, T1, T2, positions, relative_B1, ...
        theta, trajectory, G, delta_k, dwell_time)
    signal = zeros(1, length(trajectory));
    
    simulator = Simulator(M0, T1, T2, positions);
    
    simulator.pulse(theta .* relative_B1);
    
    simulator.idle(5e-3);
    
    G_max = 25e-3; % mT/m
    tau_begin = abs(trajectory(1)/(simulator.gamma*G_max));
    G_begin = trajectory(1)/(simulator.gamma*tau_begin);
    simulator.gradient(G_begin, tau_begin);
    
    for i=1:length(trajectory)
        M = simulator.magnetization(1, :) + 1j*simulator.magnetization(2, :);
        
        k = 1+round((trajectory(i) - min(trajectory))/delta_k);
        signal(k) = sum(M);
        
        if i ~= length(trajectory)
            simulator.gradient(G(i), dwell_time);
        end
    end
end
