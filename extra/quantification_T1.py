import sys

import matplotlib.pyplot
import numpy
import scipy

import style

def main():
    # Longitudinal relaxation rates, s
    R1_a, R1_b = 1/1.100, 1/0.200
    
    # Exchange constant, Hz
    R = 15
    
    # Equilibrium magnetization, unitless
    Meq_a, Meq_b = 1, 0.1
    
    # Magnetization just after the inversion pulse
    M0_a, M0_b = -Meq_a, 0
    
    t = numpy.linspace(0, 5, 50) # s
    
    numpy.random.seed(42)
    signal = (
        bi_exponential(Meq_a, Meq_b, R1_a, R1_b, R, M0_a, M0_b, t)
        + numpy.random.normal(0, 0.02, len(t)))
    
    figure, plots = matplotlib.pyplot.subplots(
        1, 3, sharex=True, sharey=True, layout="tight", figsize=(9, 3.2))
    # figure.suptitle(
    #     "Simulated values: "
    #     f"$T_{{1,a}}$={1e3/R1_a:.0f} ms, $T_{{1,b}}$={1e3/R1_b:.0f} ms, "
    #     f"$f$={Meq_b/(Meq_a+Meq_b):.2f}, $R$={R:.0f} Hz")
    
    single_pool_cost = lambda x: numpy.linalg.norm(
        single_pool(Meq_a+Meq_b, x[0], M0_a+M0_b, t)
        - signal)
    R1_opt, = scipy.optimize.minimize(single_pool_cost, [1]).x
    plots[0].plot(t, signal, label="Observed")
    plots[0].plot(
        t, single_pool(Meq_a+Meq_b, R1_opt, -1, t),
        label=f"$T_1 \\approx {1e3/R1_opt:.0f}$ ms")
    
    separate_pools_cost = lambda x: numpy.linalg.norm(
        separate_pools(Meq_a, Meq_b, x[0], x[1], M0_a, M0_b, t)
        - signal)
    R1_a_opt, R1_b_opt = scipy.optimize.minimize(
        separate_pools_cost, [1., 10.]).x
    plots[1].plot(t, signal, label="Observed")
    plots[1].plot(
        t, separate_pools(Meq_a, Meq_b, R1_a_opt, R1_b_opt, M0_a, M0_b, t),
        label=f"$T_{{1,a}} \\approx {1e3/R1_a_opt:.0f}$ ms\n"
            f"$T_{{1,b}} \\approx {1e3/R1_b_opt:.0f}$ ms\n")
    
    bi_exponential_cost = lambda x: numpy.linalg.norm(
        bi_exponential(Meq_a, Meq_b, x[0], x[1], x[2], M0_a, M0_b, t)
        - signal)
    R1_a_opt, R1_b_opt, R_opt = scipy.optimize.minimize(
        bi_exponential_cost, [1., 10., 10.]).x
    plots[2].plot(t, signal, label="Observed")
    plots[2].plot(
        t, bi_exponential(Meq_a, Meq_b, R1_a_opt, R1_b_opt, R_opt, M0_a, M0_b, t),
        label=f"$T_{{1,a}} \\approx {1e3/R1_a_opt:.0f}$ ms\n"
            f"$T_{{1,b}} \\approx {1e3/R1_b_opt:.0f}$ ms\n"
            f"$R \\approx {R_opt:.0f}$ Hz\n")
    
    titles = ["Single pool", "Two separate pools", "Two exchanging pools"]
    for plot, title in zip(plots, titles):
        plot.set(xlim=[t.min(), t.max()], xlabel="Time (s)", title=title)
        plot.legend()
    plots[0].set(ylabel=("$M_\parallel/M_0$ (unitless)"))
    
    figure.savefig("quantification_T1_fits.png")
    
    figure, plot = matplotlib.pyplot.subplots(layout="tight", figsize=(3, 3))
    for R_ in numpy.linspace(5, 50, 200):
        signal = bi_exponential(Meq_a, Meq_b, R1_a, R1_b, R_, M0_a, M0_b, t)
        plot.plot(t, signal, "black", alpha=0.1)
    plot.set(
        xlim=[t.min(), t.max()],
        xlabel="Time (s)", ylabel=("$M_\parallel/M_0$ (unitless)"))
    figure.savefig("quantification_T1_R_dependency.png")
    
    signal_raw = bi_exponential(Meq_a, Meq_b, R1_a, R1_b, R, M0_a, M0_b, t)
    runs = 300
    R1_a_opt, R1_b_opt, R_opt = numpy.empty((3, runs))
    for i in range(runs):
        noise = numpy.random.normal(0, 0.02, len(t))
        signal = signal_raw+noise
        
        R1_a_opt[i], R1_b_opt[i], R_opt[i] = scipy.optimize.minimize(
            bi_exponential_cost, [1, 10, 10],
            bounds=((0.1,100), (0.1,100), (0.1, 50))).x
    
    figure, plots = matplotlib.pyplot.subplots(
        1, 3, layout="tight", figsize=(9, 3))
    plots[0].hist(R1_a_opt, 30)
    plots[0].set(xlabel="$R_{1,a}$ (Hz)")
    
    plots[1].hist(R1_b_opt, 30)#, range=(0, 2))
    plots[1].set(xlabel="$R_{1,b}$ (Hz)")
    
    plots[2].hist(R_opt, 30)
    plots[2].set(xlabel="$R$ (Hz)")
    
    for plot in plots:
        plot.set(yticks=[])
        plot.spines[["left", "top", "right"]].set_visible(False)
    figure.savefig("quantification_T1_monte_carlo.png")

def single_pool(Meq, R1, M0, t):
    return Meq - numpy.exp(-t*R1) * (Meq - M0)

def separate_pools(Meq_a, Meq_b, R1_a, R1_b, M0_a, M0_b, t):
    return single_pool(Meq_a, R1_a, M0_a, t) + single_pool(Meq_b, R1_b, M0_b, t)

def bi_exponential(Meq_a, Meq_b, R1_a, R1_b, R, M0_a, M0_b, t):
    # Exchange rates from a to b and from b to a, Hz
    k_ab, k_ba = R*Meq_b, R*Meq_a
    
    # Short and long bi-exponential longitudinal relaxation rates
    Delta = (R1_a-R1_b + k_ab-k_ba)**2 + 4*k_ab*k_ba
    R1_S = 0.5*(R1_a+R1_b + k_ab+k_ba + numpy.sqrt(Delta))
    R1_L = 0.5*(R1_a+R1_b + k_ab+k_ba - numpy.sqrt(Delta))
    
    # Weights of the equivalent pools
    b_S = ((M0_a/Meq_a-1) * (R1_a-R1_L) + ((M0_a/Meq_a-M0_b/Meq_b) * k_ab))/(R1_S-R1_L)
    b_L = -((M0_a/Meq_a-1) * (R1_a-R1_S) + ((M0_a/Meq_a-M0_b/Meq_b) * k_ab))/(R1_S-R1_L)
    
    return 1 + b_S*numpy.exp(-t*R1_S) + b_L*numpy.exp(-t*R1_L)

if __name__ == "__main__":
    sys.exit(main())
