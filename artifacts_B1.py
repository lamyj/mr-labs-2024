import matplotlib.pyplot
import numpy

from simulator import Simulator

import style

def main():
    # Shape and positions of the objects
    radius = 0.02 # m
    centers = [-0.05, 0., 0.05] # m
    
    # Geometry of the image
    fov = 0.15 # m
    voxel_size = 1e-3 # m
    definition = int(fov/voxel_size) # voxels
    
    length = int(2*radius/voxel_size) # number of voxels in each object
    positions = numpy.concatenate([
        voxel_size*numpy.arange(-length/2, length/2)+c for c in centers]) # m
    M0 = numpy.concatenate([length*[1.], length*[0.7], length*[0.5]]) # arbitrary units
    T1 = numpy.concatenate([length*[1.], length*[0.7], length*[2.0]]) # s
    T2 = numpy.concatenate([length*[10e-3], length*[50e-3], length*[100e-3]]) # s
    
    k_max = 0.5 * 2*numpy.pi/voxel_size # rad/m
    k_definition = definition # unitless
    delta_k = 2*k_max/(k_definition-1) # rad/m
    trajectory = numpy.linspace(-k_max, k_max, k_definition) # rad/m
    
    dwell_time = 10e-6 # s
    G = numpy.diff(trajectory, axis=0)/(Simulator.gamma*dwell_time) # T/m
    
    signals = {}
    
    relative_B1 = 1
    signals[r"$rB_1=1$"] = simulate(
        M0, T1, T2, positions, relative_B1,
        numpy.radians(20), trajectory, G, delta_k, dwell_time)
    
    relative_B1 = 0.9
    signals[r"$rB_1 \ne 1$"] = simulate(
        M0, T1, T2, positions, relative_B1,
        numpy.radians(20), trajectory, G, delta_k, dwell_time)
    images = {
        k: numpy.fft.fftshift(numpy.fft.fft(numpy.fft.fftshift(v)))
        for k,v in signals.items() }
    
    figure, plot = matplotlib.pyplot.subplots(layout="tight", figsize=(2,2))
    for name, signal in signals.items():
        plot.plot(numpy.abs(images[name]), label=name)
    plot.set(ylabel="Magnitude (a.u.)")
    plot.legend(loc="lower right")
    figure.savefig("artifacts_B1_constant.png")
    
    relative_B1 = numpy.concatenate([length*[0.7], length*[1.1], length*[1.3]])
    
    signals[r"$rB_1 \ne 1$"] = simulate(
        M0, T1, T2, positions, relative_B1,
        numpy.radians(20), trajectory, G, delta_k, dwell_time)
    images = {
        k: numpy.fft.fftshift(numpy.fft.fft(numpy.fft.fftshift(v)))
        for k,v in signals.items() }
    
    figure, plot = matplotlib.pyplot.subplots(layout="tight", figsize=(2,2))
    for name, signal in signals.items():
        plot.plot(numpy.abs(images[name]), label=name)
    plot.set(ylabel="Magnitude (a.u.)")
    plot.legend(loc="lower right")
    figure.savefig("artifacts_B1_by_object.png")
    
    signals[r"$rB_1 \ne 1$"] = simulate(
        M0, T1, T2, positions, relative_B1,
        numpy.radians(40), trajectory, G, delta_k, dwell_time)
    images = {
        k: numpy.fft.fftshift(numpy.fft.fft(numpy.fft.fftshift(v)))
        for k,v in signals.items() }
    
    figure, plot = matplotlib.pyplot.subplots(layout="tight", figsize=(2,2))
    for name, signal in signals.items():
        plot.plot(numpy.abs(images[name]), label=name)
    plot.set(ylabel="Magnitude (a.u.)")
    plot.legend(loc="lower right")
    figure.savefig("artifacts_B1_double_angle.png")
    
    signals[r"$\theta$ = 20°"] = simulate(
        M0, T1, T2, positions, relative_B1,
        numpy.radians(20), trajectory, G, delta_k, dwell_time)
    signals[r"$\theta$ = 40°"] = simulate(
        M0, T1, T2, positions, relative_B1,
        numpy.radians(40), trajectory, G, delta_k, dwell_time)
    images = {
        k: numpy.fft.fftshift(numpy.fft.fft(numpy.fft.fftshift(v)))
        for k,v in signals.items() }
    
    ratio = numpy.abs(images[r"$\theta$ = 40°"])/numpy.abs(images[r"$\theta$ = 20°"])
    actual_flip_angle = numpy.arccos(ratio/2)
    relative_B1 = actual_flip_angle / numpy.radians(20)
    
    figure, plots = matplotlib.pyplot.subplots(
        1, 2, layout="tight", figsize=(4,2))
    plots[0].plot(numpy.degrees(actual_flip_angle))
    plots[0].set(xlabel="Position (voxels)", ylabel="Flip Angle (°)")
    plots[1].plot(relative_B1)
    plots[1].set(xlabel="Position (voxels)", ylabel="Relative $B_1$ (unitless)")
    figure.savefig("artifacts_B1_map.png")

def simulate(
        M0, T1, T2, positions, relative_B1,
        theta, trajectory, G, delta_k, dwell_time):
    signal = numpy.zeros(len(trajectory), complex)
    
    simulator = Simulator(M0, T1, T2, positions)
    
    simulator.pulse(relative_B1*theta)
    
    simulator.idle(5e-3)
    
    G_max = 25e-3 # mT/m
    tau_begin = numpy.abs(trajectory[0])/(simulator.gamma*G_max)
    G_begin = trajectory[0]/(simulator.gamma*tau_begin)
    simulator.gradient(G_begin, tau_begin)
    
    for i in range(len(trajectory)):
        M = simulator.magnetization[:, 0] + 1j*simulator.magnetization[:, 1]
        
        k = numpy.round((trajectory[i] - trajectory.min())/delta_k).astype(int)
        signal[k] = M.sum()
        
        if i != len(trajectory)-1:
            simulator.gradient(G[i], dwell_time)
    
    return signal

if __name__ == "__main__":
    main()
