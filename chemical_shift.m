radius = 0.025; % m
centers = [-0.05, 0., 0.05]; % m

fov = 0.15; % m
voxel_size = 1e-3; % m
definition = fov/voxel_size; % voxels

object_length = 2*radius/voxel_size; % number of voxels in each object
positions = [];
for i=1:length(centers)
    positions = [ ...
        positions, voxel_size * (-object_length/2:object_length/2-1)+centers(i)];
end

M0 = [ ...
    repelem(1, object_length), ...
    repelem(1, object_length), ...
    repelem(1, object_length)]; % arbitrary units
T1 = [ ...
    repelem(1, object_length), ...
    repelem(1, object_length), ...
    repelem(1, object_length)]; % s
T2 = [ ...
    repelem(100e-3, object_length), ...
    repelem(100e-3, object_length), ...
    repelem(100e-3, object_length)]; % s

delta_omega_fat = Simulator.gamma_bar * 1.5 * 3.5e-6; % Hz
delta_omega = [ ...
    repelem(delta_omega_fat, object_length), ...
    repelem(0, object_length), ...
    repelem(delta_omega_fat, object_length)];

k_definition = definition; % unitless
k_max = 0.5 * 2*pi/voxel_size; % rad/m
delta_k = 2*k_max/(k_definition-1); % rad/m
trajectory = linspace(-k_max, k_max, k_definition);

dwell_time = 50e-6 ; % s
G = diff(trajectory)/(Simulator.gamma*dwell_time) ; % T/m
signal_50_us = simulate( ...
    M0, T1, T2, positions, delta_omega, ...
    5e-3, trajectory, G, delta_k, dwell_time);
image_50_us = fftshift(fft(fftshift(signal_50_us)));

dwell_time = 200e-6 ; % s
G = diff(trajectory)/(Simulator.gamma*dwell_time) ; % T/m
signal_200_us = simulate( ...
    M0, T1, T2, positions, delta_omega, ...
    5e-3, trajectory, G, delta_k, dwell_time);
image_200_us = fftshift(fft(fftshift(signal_200_us)));

figure;
subplot(1, 2, 1);
plot(abs(image_50_us));
subplot(1, 2, 2);
plot(abs(image_200_us));

shift = delta_omega_fat * length(trajectory)*50e-6;
disp(['Shift with a dwell time of 50 µs: ', num2str(shift), ' pixels']);

shift = delta_omega_fat * length(trajectory)*200e-6;
disp(['Shift with a dwell time of 200 µs: ', num2str(shift), ' pixels']);

shift = 0.5; 
readout_time = shift/delta_omega_fat; % s
disp(['Readout time for a shift of 0.5 pixel: ', num2str(1e3*readout_time), ' ms']);
dwell_time = readout_time/length(trajectory); % s
disp(['Dwell time for a shift of 0.5 pixel: ', num2str(1e6*dwell_time), ' µs']);;

G = 2*k_max / Simulator.gamma / readout_time; % T/m
disp(['Gradient time for a shift of 0.5 pixel: ', num2str(1e3*G), ' mT/m']);

function signal = simulate( ...
        M0, T1, T2, positions, delta_omega, ...
        TE, trajectory, G, delta_k, dwell_time)
    signal = zeros(1, length(trajectory));
    
    simulator = Simulator(M0, T1, T2, positions, delta_omega);
    
    simulator.pulse(pi/2);
    
    simulator.idle(TE-length(trajectory)/2*dwell_time);
    
    G_max = 25e-3; % mT/m
    tau_begin = abs(trajectory(1)/(simulator.gamma*G_max));
    G_begin = trajectory(1)/(simulator.gamma*tau_begin);
    simulator.gradient(G_begin, tau_begin);
    
    for i=1:length(trajectory)
        M = simulator.magnetization(1, :) + 1j*simulator.magnetization(2, :);
        
        k = 1+round((trajectory(i) - min(trajectory))/delta_k);
        signal(k) = sum(M);
        
        if i ~= length(trajectory)
            simulator.gradient(G(i), dwell_time);
        end
    end
end
