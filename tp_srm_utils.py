import numpy as np


class SRM_utils(object):
    def read_LCMRAW(nom_fichier):
        entete = {}
        data = []
        with open(nom_fichier, 'r') as f:
            # Lire l'entête
            end_count=0
            count=0
            while end_count<2:
                line = f.readline().strip()
                count+=1
                if line.startswith("$END"):
                    end_count += 1
                elif "=" in line:
                    if line.startswith("$"):
                        key, value = line[1:].split("=")
                    else:
                        key, value = line[0:].split("=")
                    entete[key.strip()] = value.strip()

            # Lire les données
            for line in f:
                if line.strip() == '':
                    continue
                data.append([float(x) for x in line.split()])

            # Convertir en tableau NumPy
        data = np.array(data)

        # Séparer les parties réelles et imaginaires intercalées
        parties_reelles = data[0::2]
        parties_imaginaires = data[1::2]
        signal_complexe = parties_reelles + 1j * parties_imaginaires

        return entete, signal_complexe



    def write_LCMRAW(data=None, TE=None, HZPPPM=None, SEQ=None, ID=None, FMTDAT='6.3f',VOLUME=None, TRAMP=None, outputfilename=None):
        Spectrum_real = np.real(data)
        Spectrum_imag = np.imag(data)
        Spectrum_serialized = np.ravel(np.stack((Spectrum_real, Spectrum_imag), axis=1))

        Nbrfmt = '%' + FMTDAT
        #HR modif Header
        Header = "\
        $SEQPAR\n\
        ECHOT={le_TE:.16f}\n\
        HZPPPM={le_HZPPPM:.16f}\n\
        SEQ={le_SEQ}\n\
        $END\n\
        $NMID\n\
        ID='{le_ID}'\n\
        FMTDAT='(f16.3)'\n\
        VOLUME={le_VOLUME:.16f}\n\
        TRAMP={le_TRAMP:.16f}\n\
        $END".format(
        le_TE=TE,
        le_HZPPPM=HZPPPM,
        le_SEQ=SEQ,
        le_ID=ID,
        le_FMTDAT=FMTDAT,
        le_VOLUME=VOLUME,
        le_TRAMP=TRAMP)
        print(outputfilename)
        np.savetxt(outputfilename,Spectrum_serialized,fmt=Nbrfmt, delimiter='\n', newline='\n', header=Header, comments='')
        return

    def fft_signal(signal):
        return np.fft.fftshift(np.fft.fft(signal,axis=0))

    def generateLipids(dt, Fref, A, N):
        # Générer le signal des lipides
        # A amplitude pour mettre à l''echelle par rapport au spectre
        # Paramètres d'amplitude et de fréquence
        amp = A * np.array([6 * 2, 3])
        freq = np.array([Fref * (4.7 - 1.3), Fref * (4.7 - 0.9)])

        # Paramètre d'apodisation
        apo = 50

        # Temps d'échantillonnage
        tt = np.arange(0, N * dt, dt)

        # Générer le signal apodisé pour chaque composante de lipides
        aposig = amp[:, np.newaxis] * np.exp(-(apo * tt) ** 2)

        # Générer le signal des lipides
        lipi = np.zeros((2, N), dtype=complex)  # Initialisation du signal sig

        # Calcul de exp(1i*2*pi*f*t) pour chaque composante du vecteur de fréquence f
        exp_term = np.exp(1j * 2 * np.pi * freq.reshape(-1, 1) * tt)

        # Multiplication terme à terme de aposig avec exp(1i*2*pi*f*t) et sommation
        for i in range(2):  # Pour chaque composante de aposig et f
            lipi[i] = aposig[i] * exp_term[i]

        # Somme des deux composantes de sig
        lip = np.sum(lipi, axis=0)  # Somme sur l'axe 0 pour obtenir une seule composante

        return lip