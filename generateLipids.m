function lip=generateLipids(dt,Fref,A,N)
            %generate lipids signal
                     
            amp= A*[6*2 3]
            freq=[Fref*(4.7-1.3) Fref*(4.7-0.9)];
            apo=50;
            tt=[0:dt:(N-1)*dt];
            aposig=amp.'*exp(-(apo*tt).^2);
            
            lip = sum(aposig.*exp(1i*2*pi*tt.'*freq).');           
                        
 end 
           